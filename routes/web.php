<?php

use Illuminate\Support\Facades\Route;
use App\Helpers\RestrictedUser;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('htaccess/login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
Route::post('htaccess/login/attempt', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login-attempt');
Route::post('htaccess/logout', 'Auth\AdminLoginController@logout')->name('logout');

Auth::routes(['register' => false, 'login' => false]);

Route::group(['prefix' => 'htaccess', 'middleware' => 'auth'], function(){

    Route::get('dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

    Route::get('admin-users/personal-setting', [App\Http\Controllers\AdminUserController::class, 'personalSetting'])
        ->name('admin-users.personalSetting');
    
    Route::patch('admin-users/update-personal-setting', [App\Http\Controllers\AdminUserController::class, 'updatePersonalSetting'])
        ->name('admin-users.updatePersonalSetting');

    Route::get('admin-users/security-setting', [App\Http\Controllers\AdminUserController::class, 'securitySetting'])
        ->name('admin-users.securitySetting');
    
    Route::patch('admin-users/update-password', [App\Http\Controllers\AdminUserController::class, 'updatePassword'])
        ->name('admin-users.updatePassword');

        Route::get('admin-configurations/datatable', [App\Http\Controllers\AdminConfigurationController::class, 'datatable'])->name('admin-configurations.datatable');

        Route::get('admin-configurations', [App\Http\Controllers\AdminConfigurationController::class, 'index'])->name('admin-configurations.index');

        Route::get('admin-configurations/{id}/edit', [App\Http\Controllers\AdminConfigurationController::class, 'edit'])->name('admin-configurations.edit');

        Route::patch('admin-configurations/{id}/update', [App\Http\Controllers\AdminConfigurationController::class, 'update'])->name('admin-configurations.update');

        Route::get('admin-roles/datatable', [App\Http\Controllers\AdminRoleController::class, 'datatable'])
            ->name('admin-roles.datatable');
            
        Route::resource('admin-roles', 'App\Http\Controllers\AdminRoleController');

        Route::get('admin-file-views/datatable', [App\Http\Controllers\AdminFileViewController::class, 'datatable'])->name('admin-file-views.datatable');

        Route::resource('admin-file-views', 'App\Http\Controllers\AdminFileViewController');

        Route::get('admin-icons/datatable', [App\Http\Controllers\AdminIconController::class, 'datatable'])->name('admin-icons.datatable');

        Route::resource('admin-icons', 'App\Http\Controllers\AdminIconController');


        Route::get('admin-users/datatable', [App\Http\Controllers\AdminUserController::class, 'datatable'])
        ->name('admin-users.datatable');
            
        Route::resource('admin-users', 'App\Http\Controllers\AdminUserController');


    /**Transaction */
    Route::get('admin-slideshow/datatable', [App\Http\Controllers\AdminSlideshowController::class, 'datatable'])->name('admin-slideshow.datatable');

    Route::resource('admin-slideshow', 'App\Http\Controllers\AdminSlideshowController');

    Route::get('admin-ref-categories/datatable', [App\Http\Controllers\AdminRefCategoryController::class, 'datatable'])->name('admin-ref-categories.datatable');

    Route::resource('admin-ref-categories', 'App\Http\Controllers\AdminRefCategoryController');

    Route::get('admin-ref-shipping-methods/datatable', [App\Http\Controllers\AdminRefShippingMethodController::class, 'datatable'])->name('admin-ref-shipping-methods.datatable');

    Route::resource('admin-ref-shipping-methods', 'App\Http\Controllers\AdminRefShippingMethodController');
    
    Route::get('admin-products/datatable', [App\Http\Controllers\AdminProductController::class, 'datatable'])->name('admin-products.datatable');

    Route::resource('admin-products', 'App\Http\Controllers\AdminProductController');

    Route::get('admin-product-details/{product_id}/datatable', [App\Http\Controllers\AdminProductDetailController::class, 'datatable'])->name('admin-product-details.datatable');

    Route::get('admin-product-details/{product_id}', [App\Http\Controllers\AdminProductDetailController::class, 'index'])->name('admin-product-details.index');

    Route::post('admin-product-details/{product_id}/store', [App\Http\Controllers\AdminProductDetailController::class, 'store'])->name('admin-product-details.store');

    Route::get('admin-product-details/{id}/edit', [App\Http\Controllers\AdminProductDetailController::class, 'edit'])->name('admin-product-details.edit');

    Route::patch('admin-product-details/{id}/update', [App\Http\Controllers\AdminProductDetailController::class, 'update'])->name('admin-product-details.update');

    // Route::get('admin-product-details/{product_id}/{id}/show', [App\Http\Controllers\AdminProductDetailController::class, 'show'])->name('admin-product-details.show');

    Route::delete('admin-product-details/{id}/destroy', [App\Http\Controllers\AdminProductDetailController::class, 'destroy'])->name('admin-product-details.destroy');

    Route::get('admin-promotions/datatable', [App\Http\Controllers\AdminPromotionController::class, 'datatable'])->name('admin-promotions.datatable');

    Route::resource('admin-promotions', 'App\Http\Controllers\AdminPromotionController');
    
    Route::get('admin-transactions/datatable', [App\Http\Controllers\AdminTransactionController::class, 'datatable'])->name('admin-transactions.datatable');

    Route::resource('admin-transactions', 'App\Http\Controllers\AdminTransactionController');

    Route::get('admin-transaction-details/{transaction_id}/datatable', [App\Http\Controllers\AdminTransactionDetailController::class, 'datatable'])->name('admin-transaction-details.datatable');
    
});
