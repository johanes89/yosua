<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('products', [App\Http\Controllers\ApiProductController::class, 'index']);
Route::get('product-detail/{id}', [App\Http\Controllers\ApiProductController::class, 'detail']);
Route::get('product-by-category/{id}', [App\Http\Controllers\ApiProductController::class, 'productByCategory']);
Route::get('product-search/{type}/{key}', [App\Http\Controllers\ApiProductController::class, 'productSearch']);

Route::get('countries', [App\Http\Controllers\ApiReferenceController::class, 'country']);
Route::get('provinces', [App\Http\Controllers\ApiReferenceController::class, 'province']);
Route::get('regencies/{id}', [App\Http\Controllers\ApiReferenceController::class, 'regencyById']);
Route::get('districts/{id}', [App\Http\Controllers\ApiReferenceController::class, 'districtById']);
Route::get('subDistricts/{id}', [App\Http\Controllers\ApiReferenceController::class, 'subDistrictById']);
Route::get('shippingMethods', [App\Http\Controllers\ApiReferenceController::class, 'shippingMethod']);
Route::get('paymentMethods', [App\Http\Controllers\ApiReferenceController::class, 'paymentMethod']);

Route::post('checkout', [App\Http\Controllers\ApiCartController::class, 'save']);
Route::get('getTransaction/{id}', [App\Http\Controllers\ApiCartController::class, 'getTransaction']);
Route::post('paymentConfirmation/{id}', [App\Http\Controllers\ApiCartController::class, 'paymentConfirmation']);

Route::get('slideshow', [App\Http\Controllers\ApiReferenceController::class, 'slideshow']);
Route::get('categories', [App\Http\Controllers\ApiReferenceController::class, 'category']);

Route::get('configuration', [App\Http\Controllers\ApiReferenceController::class, 'configuration']);
