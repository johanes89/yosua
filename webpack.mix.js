const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/**Global */
mix.sass('resources/sass/app.scss', 'public/css')
    .options({ processCssUrls: false });

/**Frontends */
mix.copyDirectory('resources/frontends/citytours/css/fontello', 'public/css/fontello')
    .copyDirectory('resources/frontends/citytours/css/new_icons', 'public/css/new_icons');

mix.js('resources/frontends/js/app.js', 'public/js');

mix.styles([
    'resources/frontends/citytours/css/bootstrap.min.css',
    'resources/frontends/citytours/css/style.css',
    'resources/frontends/citytours/css/vendors.css',
    'resources/frontends/citytours/css/shop.css',
    'resources/frontends/citytours/css/custom.css',
    'resources/css/external-css.css'
], 'public/css/citytours-packed.css');

mix.scripts([
    'resources/frontends/citytours/js/jquery-2.2.4.min.js',
    'resources/frontends/citytours/js/common_scripts_min.js',
    'resources/frontends/citytours/js/functions.js',
    'resources/frontends/citytours/js/modernizr.js',
    'resources/frontends/citytours/js/_functions.js',
    ], 'public/js/citytours-packed.js');

// mix.scripts([
//     'resources/frontends/citytours/js/video_header.js',
//     'resources/frontends/js/functions/videoscript.js'
//     ], 'public/js/script-video-packed.js');

/** Backend */
mix.copyDirectory('resources/backends/dashlite/images', 'public/dashlite/images')
    .copyDirectory('resources/backends/dashlite/assets/fonts', 'public/fonts');

/**Dashlite */
mix.scripts([
    'resources/backends/dashlite/assets/js/bundle.min.js',
    'resources/backends/dashlite/assets/js/scripts.js',
    'resources/backends/dashlite/assets/js/libs/editors/summernote.js',
    'resources/backends/dashlite/assets/js/editors.js',
    'resources/backends/functions/_scripts.js'
], 'public/js/backend-dashlite-packed.js');

mix.styles([
    'resources/backends/dashlite/assets/css/dashlite.css'
], 'public/css/backend-dashlite-packed.css');

/**Modules */
mix.scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/select2/dist/js/select2.full.min.js',
    'node_modules/@fortawesome/fontawesome-free/js/all.min.js'
], 'public/js/modules-asset-packed.js');

mix.styles([
    'node_modules/bootstrap/scss/bootstrap.scss',
    'node_modules/select2/dist/css/select2.min.css',
    'node_modules/@fortawesome/fontawesome-free/css/all.min.css'
], 'public/css/modules-asset-packed.css');

/**Datatables */
mix.scripts([
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-responsive/js/dataTables.responsive.min.js'
], 'public/js/datatables-packed.js');

mix.scripts([
    'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
    'node_modules/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js'
], 'public/js/datatables-bs4-packed.js');

mix.styles([
    'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
    'node_modules/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css'
], 'public/css/datatables-bs4-packed.css');
