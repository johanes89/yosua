window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    // window.$ = window.jQuery = require('jquery');

    /**Additional Vendors */
    // require('select2/dist/js/select2.full.min');
    // require('datatables.net/js/jquery.dataTables.min');
    // require('datatables.net-bs4/js/dataTables.bootstrap4.min');

    // require('@fortawesome/fontawesome-free/js/all.min');


    // require('./modules/_script');

} catch (e) {}