import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        errors: [],
        confirmation_transaction_status: false,
        confirmation_transaction:{}
    },
    getters: {
    },

    actions: {
        setConfirmation({ commit }, value) {
            commit('setConfirmationTransaction', value, { root: true })
            commit('setConfirmationTransactionStatus', true, { root: true })
        },

        setConfirmationStatus({ commit }, value) {
            commit('setConfirmationTransactionStatus', value, { root: true })
        },

        setClearConfirmation({ commit }) {
            commit('clearConfirmation', { root: true })
        },
    },

    mutations: {
        setErrors(state, payload) {
            state.errors = payload;
        },

        clearErrors(state) {
            state.errors = [];
        },

        clearConfirmation(state){
            state.confirmation_transaction = {};
            state.confirmation_transaction_status = false;
        },

        setConfirmationTransactionStatus(state, value) {
            state.confirmation_transaction_status = value;
        },

        setConfirmationTransaction(state, payload) {
            state.confirmation_transaction = payload;
        },
    },
})

export default store