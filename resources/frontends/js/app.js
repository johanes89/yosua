/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');

import Frontend from './layouts/default'
import Vuex from 'vuex'
import config from './modules/config.js'
import router from './router.js'
import store from './store/store.js'
import _function from './modules/_module.js'
import _script from './modules/_function.js'

Vue.use(Vuex)

const app = new Vue({
    el: '#app',
    components: { 
        Frontend
    },
    router,
    store,
    config
});
