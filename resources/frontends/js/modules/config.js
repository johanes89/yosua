import axios from 'axios';
window.axios = axios;

window._ = require('lodash');

var protocol = window.location.protocol;
var hostname = protocol+'//'+window.location.hostname;
window.localserver = '/yosua/public'; /**Please set local path on development mode */

if (process.env.NODE_ENV === 'production') {
    // mutate config for production...
    window.url = hostname+'/';
    // window.imgUrl = hostname+'/storage/';
} else {
    // mutate for development...
    window.url = hostname+localserver+'/';
    // window.imgUrl = hostname+localserver+'/storage/';
}

try {
    window.Popper = require('popper.js').default;
    // window.$ = window.jQuery = require('jquery');


    // require('bootstrap');
} catch (e) { }

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.baseURL = window.url + "api/";

const $axios = axios.create({
    baseURL: window.axios.defaults.baseURL,
    headers: {
        Authorization: localStorage.getItem('userToken') != 'null' ? 'Bearer ' + JSON.stringify(localStorage.getItem('userToken')) : '',
        'Content-Type': 'application/json'
    }
});

export default $axios;