import Vue from 'vue'
import Router from 'vue-router'
import config from './modules/config.js'
import store from './store/store.js'

Vue.use(Router)

import Home from './pages/index'
import Product from './pages/Product'
import ProductDetail from './pages/ProductDetail'
import ShoppingCart from './pages/ShoppingCart'
import PaymentConfirmation from './pages/PaymentConfirmation'
import ConfirmationCheckout from './pages/ConfirmationCheckout'
import PaymentConfirmationStatus from './pages/PaymentConfirmationStatus'

const router = new Router({
    mode: 'history',
    base: (process.env.NODE_ENV === 'production')?'/':localserver,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/product',
            name: 'product',
            component: Product
        },
        {
            path: '/product-detail/:id',
            name: 'product-detail',
            component: ProductDetail
        },
        {
            path: '/shopping-cart',
            name: 'shopping-cart',
            component: ShoppingCart
        },
        {
            path: '/payment-confirmation',
            name: 'payment-confirmation',
            component: PaymentConfirmation
        },
        {
            path: '/confirmation-checkout',
            name: 'confirmation-checkout',
            component: ConfirmationCheckout
        },
        {
            path: '/payment-confirmation-status',
            name: 'payment-confirmation-status',
            component: PaymentConfirmationStatus
        },
    ],
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let auth = store.getters.isAuth
        if (!auth) {
            next({ name: 'home' })
        } else {
            next()
        }
    } else {
        next()
    }
});

export default router