<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="{{ !empty($configuration->description)?$configuration->description:'Company Profile' }}">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="{{ asset($configuration->iconUrl) }}">
    <link rel="icon" href="{{ asset($configuration->iconUrl) }}" type="image/x-icon">

    <!-- Page Title  -->
    <title>
        {{ !empty($configuration->name)?$configuration->name:config('app.name', 'Website') }}
    </title>
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend-dashlite-packed.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modules-asset-packed.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables-bs4-packed.css') }}" rel="stylesheet">
</head>

<body class="nk-body bg-lighter npc-default has-sidebar">
    <div class="nk-app-root">

        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            @include('backends.includes.sidebar')
            <!-- sidebar @e -->

            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                @include('backends.includes.header')
                <!-- main header @e -->

                <!-- content @s -->
                @include('backends.includes.main-content')
                <!-- content @e -->

                <!-- footer @s -->
                @include('backends.includes.footer')
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->

        </div>
        <!-- main @e -->

    </div>
    <!-- app-root @e -->
    <script src="{{ asset('js/modules-asset-packed.js') }}"></script>
    <script src="{{ asset('js/backend-dashlite-packed.js') }}"></script>
    <script src="{{ asset('js/datatables-packed.js') }}"></script>
    <script src="{{ asset('js/datatables-bs4-packed.js') }}"></script>

</body>

</html>