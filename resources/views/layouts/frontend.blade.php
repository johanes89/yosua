<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Variasi Mobil, Stir Mobil, Pelg">
    <meta name="description" content="Variasi Mobil">
    <meta name="author" content="Ansonika">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{ asset('citytours/img/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{ asset('citytours/img/apple-touch-icon-57x57-precomposed.jpg') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ asset('citytours/img/apple-touch-icon-72x72-precomposed.jpg') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ asset('citytours/img/apple-touch-icon-114x114-precomposed.jpg') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ asset('citytours/img/apple-touch-icon-144x144-precomposed.jpg') }}">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Montserrat:300,400,700" rel="stylesheet">
    
</head>
<body>

        @include('frontends.includes.preloader')
        <!-- End Preload -->

        @include('frontends.includes.layer')
        <!-- Mobile menu overlay mask -->

        <!-- Header================================================== -->
        <header>
                @include('frontends.includes.topline')
                @include('frontends.includes.container-menu')
        </header><!-- End Header -->
        
        <main>
            @include('frontends.includes.main-content')
        </main>
        <!-- End main -->
            
        @include('frontends.includes.footer')

        <div id="toTop"></div><!-- Back to top button -->
        
        @include('frontends.includes.search-menu-overlay')

        @include('frontends.includes.signin-popup')
        
    {{-- <div id="app">

    </div> --}}

    @include('frontends.includes.js-scripts')
    @stack('scripts')
	
  </body>
</html>