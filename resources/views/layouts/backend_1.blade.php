<!DOCTYPE html>
<!-- 
Template Name: Deepor - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ !empty($configuration->name)?$configuration->name:config('app.name', 'Website') }}</title>
    <meta name="description" content="{{ !empty($configuration->description)?$configuration->description:'Company Profile' }}" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset($configuration->iconUrl) }}">
    <link rel="icon" href="{{ asset($configuration->iconUrl) }}" type="image/x-icon">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    @include('backends.includes.css-scripts')

</head>

<body>
    
	<!-- HK Wrapper -->
	<div class="hk-wrapper hk-vertical-nav">

        @include('backends.includes.header')

        @include('backends.includes.sidebar')

        @include('backends.includes.setting-panel')

        <!-- Main Content -->
        <div class="hk-pg-wrapper">
            @include('backends.includes.main-content')
            
            @include('backends.includes.footer')
        </div>
        <!-- /Main Content -->

    </div>

    @include('backends.includes.js-scripts')
    @stack('scripts')

</body>

</html>