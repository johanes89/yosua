<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="{{ !empty($configuration->keywords)?$configuration->keywords:'variasi mobil' }}">
    <meta name="description" content="Variasi Mobil">
    <link rel="shortcut icon" href="{{ asset($configuration->iconUrl) }}">
    <link rel="icon" href="{{ asset($configuration->iconUrl) }}" type="image/x-icon">
    <meta name="author" content="Ansonika">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/citytours-packed.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/citytours-packed.css') }}" rel="stylesheet">

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{ asset('citytours/img/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{ asset('citytours/img/apple-touch-icon-57x57-precomposed.jpg') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ asset('citytours/img/apple-touch-icon-72x72-precomposed.jpg') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ asset('citytours/img/apple-touch-icon-114x114-precomposed.jpg') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ asset('citytours/img/apple-touch-icon-144x144-precomposed.jpg') }}">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Montserrat:300,400,700" rel="stylesheet">
    
</head>
<body>
    <div id="app">
        <frontend></frontend>
    </div>
  </body>
</html>