<div class="row gy-4">
    <div class="col-sm-3">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Category</label>
            <div class="form-control-wrap ">
                <div class="form-control-select">
                {{ Form::select(
                    'category_id',
                    $categoryOptions,
                    null,
                    array(
                        'class' => 'form-control default-01',
                        'placeholder' => 'Select Category'
                        )
                ) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Name</label>
            {{ Form::text(
                'name',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => 'Name',
                    'required' => true
                    )
            ) }}
        </div>
        @if ($errors->has('name'))
            <span class="form-note text-danger">{{ $errors->first('name') }}</span>
        @endif
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Price</label>
            {{ Form::number(
                'price',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => '100000',
                    )
            ) }}
        </div>
        @if ($errors->has('price'))
            <span class="form-note text-danger">{{ $errors->first('price') }}</span>
        @endif
    </div>
</div>
<div class="row gy-4">
    
    <div class="col-sm-4">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Discount</label>
            {{ Form::number(
                'discount',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => '30',
                    )
            ) }}
        </div>
        @if ($errors->has('discount'))
            <span class="form-note text-danger">{{ $errors->first('discount') }}</span>
        @endif
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Discount Type</label>
            <div class="form-control-wrap ">
                <div class="form-control-select">
                {{ Form::select(
                    'discount_type',
                    ['currency' => 'Currency', 'percentage' => 'Percentage'],
                    null,
                    array(
                        'class' => 'form-control default-01',
                        'placeholder' => 'Select Discount Type'
                        )
                ) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Status</label>
            <div class="form-control-wrap ">
                <div class="form-control-select">
                {{ Form::select(
                    'status',
                    ['1' => 'Active', '0' => 'Inactive'],
                    null,
                    array(
                        'class' => 'form-control default-01',
                        )
                ) }}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row gy-4">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Description</label>
            {{ Form::textarea(
                'description',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => 'Write your description about your product here...',
                    'rows' => 3
                    )
            ) }}
        </div>
    </div>
</div>