<div class="nk-content-body">
    <div class="nk-block">
        <div class="card">
            <div class="card-aside-wrap">
                @include($form)
                <div class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg" data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                    <div class="card-inner-group">
                        @include('backends.adminUsers.personal-info')
                        
                        {{--@include('backends.adminUsers.personal-account-balance')--}}
                        
                        @include('backends.adminUsers.account-setting-menu')
                        <!-- .card-inner -->
                    </div><!-- .card-inner-group -->
                </div><!-- .card-aside -->
            </div><!-- .card-aside-wrap -->
        </div>
    </div>
</div>