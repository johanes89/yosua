<div class="card-inner">
    <div class="user-account-info py-0">
        <h6 class="overline-title-alt">Account Balance</h6>
        <div class="user-balance">12.395769 <small class="currency currency-btc">USD</small></div>
        <div class="user-balance-sub">Pending <span>0.344939 <span class="currency currency-btc">USD</span></span></div>
    </div>
</div><!-- .card-inner -->