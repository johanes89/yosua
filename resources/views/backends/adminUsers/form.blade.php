<div class="row gy-4">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Name</label>
            {{ Form::text(
                'name',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => 'Name',
                    'required' => true
                    )
            ) }}
        </div>
        @if ($errors->has('name'))
            <span class="form-note text-danger">{{ $errors->first('name') }}</span>
        @endif
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="form-label" for="default-1-02">Email</label>
            {{ Form::email(
                'email',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-02',
                    'placeholder' => 'info@gmail.com',
                    'required' => true
                    )
            ) }}
        </div>
        @if ($errors->has('email'))
            <span class="form-note text-danger">{{ $errors->first('email') }}</span>
        @endif
    </div>
    
</div>