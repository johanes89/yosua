<div class="card-inner">
    <div class="user-card">
        <div class="user-avatar bg-primary">
            <img src="{{ asset($dataReference->fileUrl) }}" alt="user" class="avatar-img rounded-circle">
        </div>
        <div class="user-info">
            <span class="lead-text">{{ isset($dataReference->name)?$dataReference->name:'' }}</span>
            <span class="sub-text">{{ isset($dataReference->email)?$dataReference->email:'' }}</span>
        </div>
        <!-- <div class="user-action">
            <div class="dropdown">
                <a class="btn btn-icon btn-trigger mr-n2" data-toggle="dropdown" href="#"><em class="icon ni ni-more-v"></em></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <ul class="link-list-opt no-bdr">
                        <li><a href="#"><em class="icon ni ni-camera-fill"></em><span>Change Photo</span></a></li>
                        <li><a href="#"><em class="icon ni ni-edit-fill"></em><span>Update Profile</span></a></li>
                    </ul>
                </div>
            </div>
        </div> -->
    </div>
</div><!-- .card-inner -->