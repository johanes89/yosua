<div class="row gy-4 pt-2">
    <div class="col-sm-12">
        <table class="table table-bordered w-100 display compact">
            <tr>
                <th>Kode Transaksi</th>
                <td>{{ $dataReference->transaction_code }}</td>
                <th>Nama</th>
                <td>{{ $dataReference->firstname." ".$dataReference->lastname }}</td>
                <th>Jenis Kelamin</th>
                <td>{{ ($dataReference->gender == 'L')?'Laki-Laki':'Perempuan'  }}</td>
            </tr>
            <tr>
                <th>Telepon</th>
                <td>{{ $dataReference->phone  }}</td>
                <th>Email</th>
                <td>{{ $dataReference->email  }}</td>
                <th>Negara</th>
                <td>{{ $dataReference->country->name  }}</td>
            </tr>
            <tr>
                <th>Alamat</th>
                <td colspan="5">
                    {{ $dataReference->address  }}, 
                    {{ ($dataReference->subDistrict)?$dataReference->subDistrict->name:""  }}, 
                    {{ ($dataReference->district)?$dataReference->district->name:""  }}, 
                    {{ ($dataReference->regency)?$dataReference->regency->name:""  }}, 
                    {{ ($dataReference->province)?$dataReference->province->name:""  }}, 
                    {{ ($dataReference->country)?$dataReference->country->name:""  }}, 
                    {{ $dataReference->postalcode  }}</td>
            </tr>
            <tr>
                <th>Metode Pengiriman</th>
                <td>{{ ($dataReference->shippingMethod)?$dataReference->shippingMethod->name:""  }}</td>
                <th>Total Transaksi</th>
                <td>{{ $dataReference->grandTotalFormat  }}</td>
                <th>Metode Pembayaran</th>
                <td>{{ ($dataReference->paymentMethod)?$dataReference->paymentMethod->name:""  }}</td>
            </tr>
            <tr>
                <th>Ongkos Kirim</th>
                <td>{{ $dataReference->shippingCostFormat  }}
                <th>Total Bayar</th>
                <td>{{ $dataReference->grandTotalAndShippingCostFormat  }}
                <th>Status</th>
                <td>{{ $dataReference->displayStatus  }}
            </tr>
        </table>
    </div>
</div>
<div class="row gy-4">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Bukti Transfer</label>
            <a href="#" data-toggle="modal" data-target="#modalZoom">
                {{ $dataReference->fileImageThumb  }}
            </a>
        </div>
    </div>
</div>

<div class="row gy-4">
    <div class="col-sm-3">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Nama Ekspedisi</label>
            {{ Form::text(
                'expedition',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => 'JNE, J&T, dll',
                    )
            ) }}
        </div>
        @if ($errors->has('expedition'))
            <span class="form-note text-danger">{{ $errors->first('expedition') }}</span>
        @endif
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Biaya Kirim</label>
            {{ Form::number(
                'shipping_cost',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => '28000',
                    )
            ) }}
        </div>
        @if ($errors->has('shipping_cost'))
            <span class="form-note text-danger">{{ $errors->first('shipping_cost') }}</span>
        @endif
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="form-label" for="default-1-01">No. Resi</label>
            {{ Form::text(
                'receipt_code',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    )
            ) }}
        </div>
        @if ($errors->has('receipt_code'))
            <span class="form-note text-danger">{{ $errors->first('receipt_code') }}</span>
        @endif
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Status</label>
            {{ Form::select(
                'status',
                ['1' => 'Diproses', '2' => 'Pembayaran', '3' => 'Pengiriman', '4' => 'Selesai', '99' => 'Dibatalkan', '0' => 'Menunggu'],
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    )
            ) }}
        </div>
        @if ($errors->has('status'))
            <span class="form-note text-danger">{{ $errors->first('status') }}</span>
        @endif
    </div>
</div>

<div class="modal fade zoom" tabindex="-1" id="modalZoom">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Bukti Transfer</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                    {{ $dataReference->fileImage  }}
            </div>
            <div class="modal-footer bg-light">
                <span class="sub-text">{{ $dataReference->transaction_code }}</span>
            </div>
        </div>
    </div>
</div>