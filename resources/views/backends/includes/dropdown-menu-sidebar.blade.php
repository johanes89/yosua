<li class="nk-menu-item has-sub">
    <a href="#" class="nk-menu-link nk-menu-toggle">
        <span class="nk-menu-icon"><em class="icon ni ni-tile-thumb-fill"></em></span>
        <span class="nk-menu-text">{{ $listMenu['label'] }}</span>
    </a>
    <ul class="nk-menu-sub">
            @if(!empty($listMenu['childs']))
            @foreach($listMenu['childs'] as $listSubMenu)
                <li class="nk-menu-item">
                    @if(!empty($listSubMenu['url']))
                    <a href="{{ $listSubMenu['url'] }}" class="nk-menu-link"><span class="nk-menu-text">{{ $listSubMenu['label'] }}</span></a>
                    @endif
                </li>
            @endforeach
            @endif
    </ul><!-- .nk-menu-sub -->
</li><!-- .nk-menu-item -->