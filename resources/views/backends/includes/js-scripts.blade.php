<script src="{{ asset('js/backend.js') }}" defer></script>
<!-- Bundles of Included plugins -->
<script src="{{ asset('dashlite/assets/js/bundle.min.js') }}"></script>

<!-- Init Code for plugins and custom sctipts -->
<script src="{{ asset('dashlite/assets/js/scripts.js') }}"></script>

{{-- <script src="{{ asset('dashlite/assets/js/libs/editors/summernote.js') }}"></script> --}}

{{-- <script src="{{ asset('dashlite/assets/js/editors.js') }}"></script> --}}
