<li class="dropdown user-dropdown">
    <a href="#" class="dropdown-toggle mr-n1" data-toggle="dropdown">
        <div class="user-toggle">
            <div class="user-avatar sm">
                <em class="icon ni ni-user-alt"></em>
            </div>
            <div class="user-info d-none d-xl-block">
                <div class="user-name dropdown-indicator">{{ $profile->name }}</div>
            </div>
        </div>
    </a>
    <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
        <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
            <div class="user-card">
                <div class="user-avatar">
                    <img src="{{ asset($profile->photo_profile) }}" alt="user" class="avatar-img rounded-circle">
                </div>
                <div class="user-info">
                    <span class="lead-text">{{ $profile->name }}</span>
                    <span class="sub-text">{{ $profile->email }}</span>
                </div>
            </div>
        </div>
        <div class="dropdown-inner">
            <ul class="link-list">
                @foreach ($profileMenus as $menu)
                    {!! $menu->menus !!}
                @endforeach
            </ul>
        </div>
        <div class="dropdown-inner">
            <ul class="link-list">
                <li>
                    <a href="#" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><em class="icon ni ni-signout"></em><span>Sign out</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</li>