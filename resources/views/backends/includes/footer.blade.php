<div class="nk-footer">
    <div class="container-fluid">
        <div class="nk-footer-wrap">
            <div class="nk-footer-copyright"> 
            &copy; {{ !empty($configuration->year)?$configuration->year:'2020' }} 
            {{ !empty($configuration->copyright_label)?$configuration->copyright_label:'Copyright By' }} <a href="{{ !empty($configuration->copyright_url)?$configuration->copyright_url:'#' }}" target="_blank">{{ !empty($configuration->copyright)?$configuration->copyright:'Inkai' }}</a>
            </div>
        </div>
    </div>
</div>