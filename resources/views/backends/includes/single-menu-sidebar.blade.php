<li class="nk-menu-item">
    <a href="{{ $listMenu['url'] }}" class="nk-menu-link">
        <span class="nk-menu-icon"><i class="{{ $listMenu['icon'] }}"></i></span>
        <span class="nk-menu-text">{{ $listMenu['label'] }}</span>
    </a>
</li><!-- .nk-menu-item -->