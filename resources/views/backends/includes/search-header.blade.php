<div class="nk-header-search ml-3 ml-xl-0">
    <em class="icon ni ni-search"></em>
    <input type="text" class="form-control border-transparent form-focus-none" placeholder="Search anything">
</div>