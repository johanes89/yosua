<div class="nk-content">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">

                @yield('content')
                
            </div>
        </div>
    </div>
</div>
