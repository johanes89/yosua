<div class="row gy-4">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Judul</label>
            {{ Form::text(
                'name',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => 'Name',
                    'required' => true
                    )
            ) }}
        </div>
    </div>
</div>
<div class="row gy-4">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="form-control-wrap">
                <label class="form-label">Image</label>
                <div class="custom-file">
                    <input type="file" name="photo_url" multiple class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">jpg, png</label>
                </div>
                @if ($errors->has('photo_url'))
                    <span class="form-note text-danger">{{ $errors->first('photo_url') }}</span>
                @endif
            </div>
        </div>
    </div>
</div>