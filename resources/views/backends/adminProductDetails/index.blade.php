@extends('layouts.backend')

@section('content')
    @include('backends.includes.notification-message')

    @if(isset($action) && !empty($action))
        @include($action)
    @endif

    @if(isset($table) && !empty($table))

        @include($table)
        
    @endif

@endsection
