<div class="white_bg">
    <div class="container margin_60">
        <div class="main_title">
            <h2>Other <span>Popular</span> tours</h2>
            <p>
                Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.
            </p>
        </div>
        <div class="row add_bottom_45">
            <div class="col-lg-4 other_tours">
                <ul>
                    <li><a href="#"><i class="icon_set_1_icon-3"></i>Tour Eiffel<span class="other_tours_price">$42</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-30"></i>Shopping tour<span class="other_tours_price">$35</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-44"></i>Versailles tour<span class="other_tours_price">$20</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-3"></i>Montparnasse skyline<span class="other_tours_price">$26</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-44"></i>Pompidue<span class="other_tours_price">$26</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-3"></i>Senna River tour<span class="other_tours_price">$32</span></a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 other_tours">
                <ul>
                    <li><a href="#"><i class="icon_set_1_icon-1"></i>Notredame<span class="other_tours_price">$48</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-4"></i>Lafaiette<span class="other_tours_price">$55</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-30"></i>Trocadero<span class="other_tours_price">$76</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-3"></i>Open Bus tour<span class="other_tours_price">$55</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-30"></i>Louvre museum<span class="other_tours_price">$24</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-3"></i>Madlene Cathedral<span class="other_tours_price">$24</span></a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 other_tours">
                <ul>
                    <li><a href="#"><i class="icon_set_1_icon-37"></i>Montparnasse<span class="other_tours_price">$36</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-1"></i>D'Orsey museum<span class="other_tours_price">$28</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-50"></i>Gioconda Louvre musuem<span class="other_tours_price">$44</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-44"></i>Tour Eiffel<span class="other_tours_price">$56</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-50"></i>Ladefanse<span class="other_tours_price">$16</span></a>
                    </li>
                    <li><a href="#"><i class="icon_set_1_icon-44"></i>Notredame<span class="other_tours_price">$26</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- End row -->

        <div class="banner colored">
            <h4>Discover our Top tours <span>from $34</span></h4>
            <p>
                Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
            </p>
            <a href="single_tour.html" class="btn_1 white">Read more</a>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-md-6 text-center">
                <p>
                    <a href="#"><img src="{{ asset('citytours/img/bus.jpg') }}" alt="Pic" class="img-fluid"></a>
                </p>
                <h4><span>Sightseen tour</span> booking</h4>
                <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                </p>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <p>
                    <a href="#"><img src="{{ asset('citytours/img/transfer.jpg') }}" alt="Pic" class="img-fluid"></a>
                </p>
                <h4><span>Transfer</span> booking</h4>
                <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                </p>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <p>
                    <a href="#"><img src="{{ asset('citytours/img/guide.jpg') }}" alt="Pic" class="img-fluid"></a>
                </p>
                <h4><span>Tour guide</span> booking</h4>
                <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                </p>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <p>
                    <a href="#"><img src="{{ asset('citytours/img/hotel.jpg') }}" alt="Pic" class="img-fluid"></a>
                </p>
                <h4><span>Hotel</span> booking</h4>
                <p>
                    Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                </p>
            </div>
        </div>
        <!-- End row -->
        
    </div>
    <!-- End container -->
</div>