@extends('layouts.frontend')

@section('content')
    @include('frontends.home.header-video')
    @include('frontends.home.container-grid')
    @include('frontends.home.container-white-bg')
    @include('frontends.home.promotion')
    @include('frontends.home.container-block')
@endsection
