<section class="header-video">
    <div id="hero_video">
        <div class="intro_title">
            <h3 class="animated fadeInDown">Affordable Paris tours</h3>
            <p class="animated fadeInDown">City Tours / Tour Tickets / Tour Guides</p>
            <a href="#" class="animated fadeInUp button_intro">View Tours</a>
            <a href="#" class="animated fadeInUp button_intro outline hidden-sm hidden-xs">View Tickets</a>
            <a href="https://www.youtube.com/watch?v=Zz5cu72Gv5Y" class="video animated fadeInUp button_intro outline">Play video</a>
        </div>
    </div>
    <div id="search_bar_container" style="z-index:9999">
        <div class="container">
            <div class="search_bar">
                <span class="nav-facade-active" id="nav-search-in">
                <span id="nav-search-in-content" style="">All tours</span>
                <span class="nav-down-arrow nav-sprite"></span>
                    <select title="Search in" class="searchSelect" id="searchDropdownBox" name="tours_category">
                        <option value="All Tours"  title="All Tours">All Tours</option>
                        <option value="Museums" title="Museums">Museums</option>
                        <option value="Tickets" title="Tickets">Tickets</option>
                        <option value="Hotels" title="Hotels">Hotels</option>
                        <option value="Restaurants" title="Restaurants">Restaurants</option>
                    </select>
                </span>
                <div class="nav-searchfield-outer">
                    <input type="text" autocomplete="off" name="field-keywords" placeholder="Type your search terms ...." id="twotabsearchtextbox">
                </div>
                <div class="nav-submit-button">
                    <input type="submit" title="Cerca" class="nav-submit-input" value="Search">
                </div>
            </div>
            <!-- End search bar-->
        </div>
    </div>
    <!-- /search_bar-->
    <img src="" alt="Image" class="header-video--media" data-video-src="" data-teaser-source="video/paris" data-provider="Youtube" data-video-width="854" data-video-height="480">
</section>
<!-- End Header video -->