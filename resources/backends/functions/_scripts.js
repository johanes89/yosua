$(document).ready(function () {
    var now = new Date();

    function generateDatePicker() {
        // Date picker
        $(".datepicker").each(function () {
            var date = $(this);

            date.datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy',
                // format: 'yyyy-mm-dd'
            });

            // check if date element has empty value
            if (date.val() == "") {
                // set default date to today
                date.datepicker(
                    "update",
                    now
                );
            }
        });
    }

    function generateDateTimePicker() {
        $("#datetimepicker").each(function () {
            var datetime = $(this);

            datetime.datetimepicker({
                autoclose: true,
                format: 'dd-mm-yyyy hh:ii'
            });

            if (datetime.val() == "") {
                datetime.datetimepicker(
                    "update",
                    now
                );
            }

        });

        $("#datetimepicker2").each(function () {
            var datetime2 = $(this);

            datetime2.datetimepicker({
                autoclose: true,
                format: 'dd-mm-yyyy hh:ii'
            });

            if (datetime2.val() == "") {
                datetime2.datetimepicker(
                    "update",
                    now
                );
            }

        });
        // $('#datetimepicker').datetimepicker({
        //     format: 'dd-mm-yyyy hh:ii:ss'
        // });
    }

    function generateMonthlyPicker() {
        var now2 = new Date();
        // Date picker
        $(".datepicker2").each(function () {
            var date = $(this);

            date.datepicker({
                autoclose: true,
                format: 'yyyy-mm',
            });

            // check if date element has empty value
            if (date.val() == "") {
                // set default date to today
                date.datepicker(
                    "update",
                    now2
                );
            }
        });
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function generateDataTable() {
        $('.datatable').each(function () {
            var table = $(this);

            // check if we allready initiate datatable
            // for this element
            if (!$.fn.DataTable.isDataTable(table)) {
                var tableChild = table.DataTable({
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    ordering: true,
                    searching: true, 
                    ajax: table.data('source'),
                    columns: table.data('columns'),
                });

                table.on("click", ".delete-button", function (e) {
                    var button = $(this);
                    button.addClass('disabled');

                    if (confirm("Yakin akan memproses tombol ini?")) {
                        $.ajax({
                            url: button.attr('href'),
                            type: 'POST', // user.destroy
                            dataType: "json",
                            data: {
                                '_method': 'DELETE',
                            },
                            success: function (result) {
                                // Do something with the result
                                location.reload();
                            }
                        });
                    } else {
                        button.removeClass('disabled');
                    }

                    e.preventDefault();
                });

                table.on("click", ".confirm-button", function (e) {
                    var button = $(this);
                    button.addClass('disabled');

                    if (confirm("Yakin akan memproses tombol ini?")) {
                        $.ajax({
                            url: button.attr('href'),
                            type: 'GET', // user.destroy
                            dataType: "json",
                            data: {
                                '_method': 'GET',
                            },
                            success: function (result) {
                                // Do something with the result
                                location.reload();
                            }
                        });
                    } else {
                        button.removeClass('disabled');
                    }

                    e.preventDefault();
                });

                table.on("click", ".modal-ajax", function (e) {
                    var button = $(this);

                    $.ajax({
                        url: button.attr('href'),
                        type: 'GET', // user.destroy
                        dataType: "json",
                        data: {
                            '_method': 'GET',
                        },
                        success: function (response) {
                            // Do something with the result
                            $('.modal-title').html(response.title);
                            $('.modal-body').html(response.body);
                        }
                    });

                    e.preventDefault();
                });
            }
        });

    }

    function generateDataTableToggle() {
        $('.datatableToggle').each(function () {
            var table = $(this);

            // check if we allready initiate datatable
            // for this element
            if (!$.fn.DataTable.isDataTable(table)) {
                var tableChild = table.DataTable({
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    ajax: table.data('source'),
                    columns: table.data('columns'),
                });

                table.on("click", ".delete-button", function (e) {
                    var button = $(this);
                    button.addClass('disabled');

                    if (confirm("Are you sure?")) {
                        $.ajax({
                            url: button.attr('href'),
                            type: 'POST', // user.destroy
                            dataType: "json",
                            data: {
                                '_method': 'DELETE',
                            },
                            success: function (result) {
                                // Do something with the result
                                location.reload();
                            }
                        });
                    } else {
                        button.removeClass('disabled');
                    }

                    e.preventDefault();
                });

                table.on('click', 'td.details-control', function () {
                    // alert('test');
                    var tr = $(this).closest('tr');
                    var row = tableChild.row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                    }
                });
            }
        });

    }

    function generateDatatableReport() {
        $('.datatableReport').DataTable();
    }

    function generateSelect2() {
        $('.select2').select2()
    }

    function generateIconSelect2(){
        $("#template-icon").select2({
            templateResult: _ui_icon_template
        });
    }

    function _ui_icon_template(icon)
    {
        if (!icon.id) { return icon.text; }
        var $spanIcon = $(
         "<span ><i class='"+icon.element.value+"'></i> " + icon.text + "</span>"
        );
        console.log(icon.element.value);
        return $spanIcon;
    }

    function generateSelect2Ajax() {
        $('.select2-ajax').each(function () {
            var obj = $(this);
            obj.select2({
                ajax: {
                    url: obj.data('url'),
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        // Tranforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data,
                        };
                    },
                    placeholder: obj.attr('placeholder'),
                    minimumInputLength: 3,
                }
            });
        });
    }

    function disableMultipleSubmit() {
        $('.submit-button').click(function (e) {
            var button = $(this);
            var form = button.closest('form');

            e.preventDefault();

            // if submit button already pressed
            // then disable button
            if (!button.hasClass('disabled')) {
                form.submit();
                // to prevent multiple sending post to the server
                button.attr('disabled', 'disabled');
                button.addClass('disabled');
            }
        });
    }

    // function generateSummernote(){
    //     $('#summernote').summernote();
    // }

    generateDataTable();
    // generateSummernote();
    // generateSelect2();
    // generateIconSelect2();
    // generateDataTableToggle();
    // generateDatatableReport();
    // generateSelect2();
    // generateSelect2Ajax();
    // generateDatePicker();
    // generateMonthlyPicker();
    // disableMultipleSubmit();
    // generateDateTimePicker();

    /*
    $(document).pjax('a.data-pjax', '#pjax-container')
    $(document).on('pjax:complete', function () {
    	generateDataTable();
    	generateSelect2();
    	generateDatePicker();
    	disableMultipleSubmit();
    });
    */

    /*
    $(document).on('submit', 'form', function (event) {
    	$.pjax.submit(event, '#pjax-container')
    });
    */

    // alert('test');

});