<?php

namespace App\Providers;

use App\Http\View\Composers\ConfigurationComposer;
use App\Http\View\Composers\SidebarComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $composerLists = collect([
            'layouts.backend' => ConfigurationComposer::class,
            'backends.includes.header' => ConfigurationComposer::class,
            'backends.includes.sidebar' => SidebarComposer::class,
            'backends.includes.footer' => ConfigurationComposer::class,
            'layouts.frontend-vue' => ConfigurationComposer::class,
        ]);
        // Using class based composers...
        foreach ($composerLists as $view => $composer) {
            View::composer(
                $view, 
                $composer
            );
        }

    }
}