<?php

namespace App\Traits;

trait SaveableTrait
{

    public function saveFromRequest($request)
    {
        // try to validate the data
        // before save process
        if (!empty($this->validations)) {
            $request->validate(
                $this->validations
            );
        }

        // fill model periode data from request
        $this->fill(
            $this->composeDataFromRequest(
                $request
            )
        );
        
        $this->save();

        return $this;
    }

    public function composeDataFromRequest($request)
    {
        return $request->except('_token');
    }

}