<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;

class ShippingMethod extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table = 'ref_shipping_methods';
    protected $fillable = [
        'name',
    ];
    protected $validations = [
		'name' => 'required|string'
	];

    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }
}
