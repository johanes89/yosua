<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaritalStatus extends Model
{
    protected $table= 'ref_marital_Status';
    protected $fillable = [
        'name'
    ];
    
    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }

}
