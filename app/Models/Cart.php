<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;
use App\Helpers\Media;
use Html;
use Illuminate\Support\Str;

class Cart extends Model
{
    use FormAccessible;
    use SaveableTrait;

    const DEFAULT_PATH_IMAGE = 'images/image-placeholder.jpg';

    protected $table="carts";
    protected $fillable =[
        'product_detail_id',
        'qty',
        'ip_address',
        'user_agent'
    ];

    protected $appends = ['first_image_detail', 'first_id', 'price_format', 'category_name', 'option_product_detail'];

    public static function scopeActive($q)
    {
        return $q->where(
            'status',
            '=',
            self::ACTIVE
        );
    }

    public function getDisplayStatusAttribute()
    {
        $status = 'Inactive';
        if($this->status == self::ACTIVE){
            $status = 'Active';
        }

        return $status;
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function productDetail()
    {
        return $this->hasMany(ProductDetail::class, 'product_id');
    }

    public function getFirstImageDetailAttribute()
    {
        return $this->productDetail->first()->fileUrl;
    }

    public function getFirstIdAttribute()
    {
        return $this->productDetail->first()->id;
    }

    public function getCategoryNameAttribute()
    {
        return $this->category->name;
    }

    public static function formatRupiah($value)
    {
        $hasil_rupiah = "Rp " . number_format($value, 2, ',', '.');
        return $hasil_rupiah;
    }

    public static function discountPrice($price, $discount, $discountType)
    {
        $discountPrice = 0;
        if(!empty($discount)){
            $discountPrice = self::formatRupiah($price-$discount);
            if($discountType == 'percentage'){
                $discountPrice = self::formatRupiah($price-($price*($discount/100)));
            }
        }
        return $discountPrice;
    }

    public function getPriceFormatAttribute()
    {
        return $this->formatRupiah($this->price);
    }

    public function getOptionProductDetailAttribute()
    {
        return $this->productDetail->map(function($detail){
            return [
                'text' => $detail->productDetailName,
                'value' => $detail->id

            ];
        });
    }
    
    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');

        return $data;
    }
}
