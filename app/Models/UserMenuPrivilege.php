<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class UserMenuPrivilege extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table = 'user_menu_privileges';
    protected $fillable = [
        'id',
        'user_id',
        'menu_id'
    ];

    public function groupMenu()
    {
        return $this->belongsTo(GroupMenu::class, 'menu_id');
    }

    public function user()
    {
        return $this->belongsTo(AdminUser::class, 'user_id');
    }

}