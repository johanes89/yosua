<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;
use App\Helpers\Media;
use Html;

class Slideshow extends Model
{
    use FormAccessible;
    use SaveableTrait;

    const DEFAULT_PATH_IMAGE = 'images/image-placeholder.jpg';

    const ACTIVE = 1;
    const INACTIVE = 0;

    protected $table="slideshow";
    protected $fillable =[
        'name',
        'photo_url'
    ];

    protected $appends = ['file_url'];
    
    public function getFileImageThumbAttribute()
    {
        return Html::image($this->fileUrl, null, ['height' => '50px']) ?? null;
    }

    public function getFileImageUrlAttribute()
    {
        return Html::image($this->fileUrl, null, ['height' => '100px']) ?? null;
    }

    public function getFileUrlAttribute()
    {
        if ((!empty($this->photo_url)) && Media::fileExists($this->photo_url)){

            $file = 'storage/'.$this->photo_url;

        } else {

            $file = self::DEFAULT_PATH_IMAGE;

        }
        return Media::getImageUrl($file);
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');

        $path = 'slideshow';
        if($request->hasFile('photo_url')){
            
            $image_url = Media::imageUploadCrop ($path, 1450, 750, $request->file('photo_url'), 1450, 750);
            $data['photo_url'] = $image_url;
        }

        return $data;
    }
}
