<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;
use App\Traits\Uuid;
use Illuminate\Support\Str;
use App\Helpers\Media;
use Html;

class Transaction extends Model
{
    use FormAccessible;
    use SaveableTrait;
    use Uuid;

    const DEFAULT_PATH_IMAGE = 'images/image-placeholder.jpg';

    const WAITING = 0;
    const PROCCESSED = 1;
    const PAYMENT = 2;
    const SHIPPING = 3;
    const FINISH = 4;
    const CANCELED = 99;

    protected $table="transactions";
    protected $fillable =[
        'uuid',
        'transaction_code',
        'firstname',
        'lastname',
        'gender',
        'phone',
        'email',
        'country_id',
        'province_id',
        'regency_id',
        'district_id',
        'sub_district_id',
        'address',
        'postalcode',
        'shipping_method_id',
        'expedition',
        'shipping_cost',
        'receipt_code',
        'grand_total',
        'payment_method_id',
        'payment_slip',
        'status'
    ];

    protected $appends = ['grand_total_format', 'grand_total_and_shipping_cost_format', 'shipping_cost_format', 'display_status', 'file_image_thumb', 'file_image'];

    public function getFileImageThumbAttribute()
    {
        return Html::image($this->fileUrl, null, ['height' => '100px']) ?? null;
    }

    public function getFileImageAttribute()
    {
        return Html::image($this->fileUrl, null, ['width' => '640']) ?? null;
    }

    public function getFileUrlAttribute()
    {
        if ((!empty($this->payment_slip)) && Media::fileExists($this->payment_slip)){

            $file = 'storage/'.$this->payment_slip;

        } else {

            $file = self::DEFAULT_PATH_IMAGE;

        }
        return Media::getImageUrl($file);
    }

    public function getGrandTotalFormatAttribute()
    {
        return Product::formatRupiah($this->grand_total);
    }

    public function getGrandTotalAndShippingCostFormatAttribute()
    {
        return Product::formatRupiah($this->grand_total+$this->shipping_cost);
    }

    public function getShippingCostFormatAttribute()
    {
        return Product::formatRupiah($this->shipping_cost);
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class, 'regency_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function subDistrict()
    {
        return $this->belongsTo(SubDistrict::class, 'sub_district_id');
    }

    public function shippingMethod()
    {
        return $this->belongsTo(ShippingMethod::class, 'shipping_method_id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id');
    }

    public function transactionDetail()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_id');
    }

    public function getDisplayStatusAttribute()
    {
        $status = 'Menunggu';
        if($this->status == self::FINISH){
            $status = 'Selesai';
        }else if($this->status == self::PROCCESSED){
            $status = 'Diproses';
        }else if($this->status == self::PAYMENT){
            $status = 'Pembayaran';
        }else if($this->status == self::SHIPPING){
            $status = 'Pengiriman';
        }else if($this->status == self::CANCELED){
            $status = 'Dibatalkan';
        }

        return $status;
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');

        $path = 'transactions';
        if($request->hasFile('payment_slip')){
            
            $image_url = Media::imageUploadCrop ($path, 640, 640, $request->file('payment_slip'), 640, 640);
            $data['payment_slip'] = $image_url;
        }

        return $data;
    }
}
