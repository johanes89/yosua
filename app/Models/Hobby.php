<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;
use App\Traits\SaveableTrait;

class Hobby extends Model
{
    use FormAccessible;
	use SaveableTrait;

	protected $table = 'ref_hobbies';
	protected $fillable = [
		'name'
	];

	protected $validations = [
		'name' => 'required',
		
    ];
    public static function asDropdownOptions()
	{
		return self::pluck(
			'name', 
			'id'
		)->all();
	}
}
