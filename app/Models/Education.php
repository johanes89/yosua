<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $table="ref_educations";
    protected $fillable =[
        'name'
    ];
    
    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }
}
