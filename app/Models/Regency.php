<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class Regency extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table = 'ref_regencies';
    protected $fillable = [
        'province_id',
        'name',
    ];

    protected $validations = [
        'name' => 'required',
    ];

    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        
        return $data;
    }

}