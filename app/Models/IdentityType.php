<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IdentityType extends Model
{
    protected $table = 'ref_identity_types';
    protected $fillable = [
        'name',
    ];

    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }

}
