<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    protected $table = 'ref_occupations';
    protected $fillable = [
        'name',
    ];

    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }
}
