<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $table= 'ref_salaries';
    protected $fillable = [
        'name'
    ];

    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }
}
