<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table = 'ref_payment_methods';
    protected $fillable = [
        'name',
    ];

    protected $validations = [
        'name' => 'required',
    ];

    const CASH = 1;
    const EDC_MANDIRI = 2;
    const OTHERS = 3;

    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }

    public function getMultiplierAttribute()
    {
        $paymentMultiplier = false;

        if ($this->id == self::EDC_MANDIRI) {
            $paymentMultiplier = PointMultiplier::EDC_MANDIRI;
        }

        return $paymentMultiplier;
    }

    public function getDisplayMultiplierAttribute()
    {
        $multiplier = $this->multiplier;
        return ($multiplier == PointMultiplier::EDC_MANDIRI ? "EDC MANDIRI" : "");
    }
}
