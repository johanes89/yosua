<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;

class Category extends Model
{
    use FormAccessible;
    use SaveableTrait;

    const ACTIVE = 1;
    const INACTIVE = 0;

    protected $table="ref_categories";
    protected $fillable =[
        'name',
        'status'
    ];
    
    public static function scopeActive($q)
    {
        return $q->where(
            'status',
            '=',
            self::ACTIVE
        );
    }

    public function getDisplayStatusAttribute()
    {
        $status = 'Inactive';
        if($this->status == self::ACTIVE){
            $status = 'Active';
        }

        return $status;
    }
    
    public static function asDropdownOptions()
    {
        return self::active()->pluck('name', 'id')->all();
    }
}
