<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table = 'ref_provinces';
    protected $fillable = [
        'name',
    ];

    protected $validations = [
        'name' => 'required',
    ];

    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        
        return $data;
    }

}