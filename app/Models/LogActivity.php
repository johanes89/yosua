<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class LogActivity extends Model
{
    use FormAccessible;
    use SaveableTrait;
    use SoftDeletes;

    protected $table = 'log_activities';
    protected $fillable = [
        'user',
        'type',
        'action',
        'table',
        'activity',
        'ip_address',
        'user_agent',
    ];

    protected $dates = ['deleted_at'];

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        return $data;
    }

}