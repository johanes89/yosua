<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Helpers\Media;
use Storage;
use Image;
use File;
use Html;
use Embed;

class GroupContent extends Model
{
    use FormAccessible;
    use SaveableTrait;

    const DEFAULT_PATH_IMAGE = 'images/image-placeholder.jpg';
    const UNPUBLISHED = 0;
    const PUBLISHED = 1;

    protected $table = 'group_contents';
    protected $fillable = [
        'group_menu_id',
        'title',
        'description',
        'image_url',
        'video_url',
        'document_url',
        'author',
        'slug',
        'lang',
    ];

    protected $cast = [
        'publish' => 'boolean'
    ];

    protected $dates = ['deleted_at'];

    public static function asDropdownOptions()
    {
        return self::whereNull('parent_id')->pluck('label', 'id')->all();
    }

    public function getVideoHtmlAttribute()
    {
        $menu = GroupMenu::find($this->groupMenu->id);

        return Media::embedVideo($this->video_url, $menu->video_width, $menu->video_height);
    }

    public function getVideoThumbAttribute()
    {
        return Media::embedVideo($this->video_url, 100, null);
    }

    public function getDocumentHtmlAttribute()
    {
        $download = '';
        if(!empty($this->document_url)){
            $download = "<a target='_blank' href=".url('storage/'.$this->document_url)."><i class='fas fa-file-download'></i> download</a>";
        }

        return $download;
    }

    public function groupMenu()
    {
        return $this->belongsTo(GroupMenu::class, 'group_menu_id');
    }

    public static function setFileUpload($mediaCategory)
    {
        $fileUpload = true;

        if(empty($mediaCategory)){
            $fileUpload = false;
    
        }else if($mediaCategory == 'none'){
            $fileUpload = false;

        }else if($mediaCategory == 'video'){
            $fileUpload = false;
        }

        return $fileUpload;
    }

    public function getFileImageThumbAttribute()
    {
        return Html::image($this->fileUrl, null, ['height' => '50px']) ?? null;
    }

    public function getFileImageUrlAttribute()
    {
        return Html::image($this->fileUrl, null, ['height' => '100px']) ?? null;
    }

    public function getFileUrlAttribute()
    {
        if ((!empty($this->image_url)) && Media::fileExists($this->image_url)){

            $file = 'storage/'.$this->image_url;

        } else {

            $file = self::DEFAULT_PATH_IMAGE;

        }
        return Media::getImageUrl($file);
    }

    public function getStatusPublishAttribute()
    {
        $status = 'Waiting';

        if($this->publish == self::PUBLISHED){
            $status = 'Published';
        }

        return $status;
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        $menu = GroupMenu::find($request->group_menu_id);
        $path = $menu->url;
        if($request->hasFile('image_url')){
            
            $image_url =Media::imageUpload($path, $menu->image_width, $menu->image_height, $request->file('image_url'));
            $data['image_url'] = $image_url;
        }

        if($request->hasFile('document_url')){
            
            $document_url = Media::documentUpload($path, $request->file('document_url'));
            $data['document_url'] = $document_url;

        }

        $data['slug'] = Str::of($request->title)->slug('-');
        $data['lang'] = 'id';
        return $data;
    }

}