<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;

class Policy extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table="policies";
    protected $fillable =[
        'name',
        'description'
    ];
    protected $validations = [
		'name' => 'required|string'
	];
    
    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }
}
