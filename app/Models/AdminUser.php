<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\SaveableTrait;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Media;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Html;
use Storage;
use Image;
use File;

class AdminUser extends Authenticatable
{
    use HasFactory, Notifiable;
    use SaveableTrait;
    
    const DEFAULT_PATH_IMAGE = 'images/emptyportrait150.png';

    const SUSPENDED = 99;
    const ACTIVE = 1;
    const WAITING = 2;
    const INACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'password',
        'profile_photo_url',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function scopeInactive($q)
    {
        return $q->where(
            'status',
            '=',
            self::INACTIVE
        );
    }

    public static function scopeActive($q)
    {
        return $q->where(
            'status',
            '=',
            self::ACTIVE
        );
    }

    public static function scopeWaiting($q)
    {
        return $q->where(
            'status',
            '=',
            self::WAITING
        );
    }

    public static function scopeSuspended($q)
    {
        return $q->where(
            'status',
            '=',
            self::SUSPENDED
        );
    }

    public function getDisplayStatusAttribute()
    {
        $status = 'Inactive';

        if($this->status == self::ACTIVE){
            $status = 'Active';
        }else if($this->status == self::WAITING){
            $status = 'Waiting';
        }else if($this->status == self::SUSPENDED){
            $status = 'Suspended';
        }

        return $status;
    }

    public function getFileImageThumbAttribute()
    {
        return Html::image($this->fileUrl, null, ['height' => '50px']) ?? null;
    }

    public function getFileUrlAttribute()
    {
        if ((!empty($this->profile_photo_url)) && Media::fileExists($this->profile_photo_url)){

            $file = 'storage/'.$this->profile_photo_url;

        } else {

            $file = self::DEFAULT_PATH_IMAGE;

        }
        return Media::getImageUrl($file);
    }

    public static function randomPassword()
    {
        $password = Str::random(8);
        return $password;
    }

    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            'user_roles',
            'user_id',
            'role_id'
        )->withTimestamps();
    }

    public function userMenuPrivileges()
    {
        return $this->belongsToMany(
            GroupMenu::class,
            'user_menu_privileges',
            'user_id',
            'menu_id'
        )->withTimestamps();
    }

    public function userRole()
    {
        return $this->hasOne(UserRole::class, 'user_id');
    }

    public function getIsOwnerAttribute()
    {
        return $this->roles()
            ->owner()
            ->exists();
    }

    public function getIsAdministratorAttribute()
    {
        return $this->roles()
            ->administrator()
            ->exists();
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getDisplayUserAttribute()
    {
        return $this->name;
    }

    public static function checkMatchArray($array, $checkedId)
    {
        return Arr::has($array, $checkedId);
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        $path = "users";

        if($request->hasFile('profile_photo_url')){
            $image_url = Media::imageUpload($path, 640, 640, $request->file('profile_photo_url'));
            $data['profile_photo_url'] = $image_url;
        }

        return $data;
    }
}