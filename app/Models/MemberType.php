<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class MemberType extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table = 'ref_member_types';
    protected $fillable = [
        'name',
    ];

    protected $validations = [
        'name' => 'required',
    ];

    const SILVER = 1;
    const GOLD = 2;
    const DIAMOND = 3;

    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }

}
