<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use FormAccessible;
    use SaveableTrait;

    const OWNER = 1;
    const ADMINISTRATOR = 2;

    protected $table = 'roles';
    protected $fillable = [
        'name',
    ];

    protected $validations = [
        'name' => 'required',
    ];

    public static function asDropdownOptions()
    {
        $user = auth()->user();

        $roles = self::where('id', '>', self::OWNER)->orderBy('id', 'desc')->pluck('name', 'id')->all();


        return $roles;
    }

    public static function asDropdownOptionsUser()
    {
        return self::where('id', '>', 2)->where('id', '<=', 4)->pluck('name', 'id')->all();
    }

    public function scopeOwner($q)
    {
        return $q->where('roles.id', '=', self::OWNER);
    }

    public function scopeAdministrator($q)
    {
        return $q->where('roles.id', '=', self::ADMINISTRATOR);
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        
        return $data;
    }

}