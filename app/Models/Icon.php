<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Icon extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table = 'icons';
    protected $fillable = [
        'name',
        'font_code',
    ];

    protected $validations = [
		'name' => 'required'
	];

    public static function asDropdownOptions()
    {
        return self::get()->pluck('name', 'font_code')->all();
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        return $data;
    }

}