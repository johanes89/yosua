<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;

class Prefix extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table="ref_prefix";
    protected $fillable =[
        'shortname',
        'name'
    ];
    protected $validations = [
		'shortname' => 'required|string',
		'name' => 'required|string'
    ];
    
    public static function asDropdownOptions()
    {
        return self::pluck('shortname', 'id')->all();
    }
}
