<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdminConfigurationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);

        return [
            'name' => [
                'max:50',
                'string',
            ],
            'logo' => [
                'nullable',
                // 'image',
                // 'mimes: jpg, jpeg, png, webp',
                // 'max:6000',
            ],
            'small_logo' => [
                'nullable',
                // 'image',
                // 'mimes: jpg, jpeg, png, webp',
                // 'max:6000',
            ],
            'icon' => [
                'nullable',
                // 'image',
                // 'mimes: jpg, jpeg, png, webp',
                // 'max:6000',
            ],
            'official_email' => [
                'nullable', 
                'string', 
                'email', 
                'max:255', 
            ],
            'copyright_url' => [
                'nullable',
                'url'
            ],
            'year' => [
                'nullable',
                'digits:4'
            ]
        ];
    }
}
