<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdminMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);

        return [
            'code' => [
                'nullable',
                'string',
                Rule::unique('members', 'code')->ignore($id),
            ],
            'firstname' => [
                'required',
                'string',
            ],
            'identity_number' => [
                'nullable',
                'numeric',
                Rule::unique('members', 'identity_number')->ignore($id),
            ],
            'phone' => [
                'required',
                'numeric',
                Rule::unique('members', 'phone')->ignore($id),
            ],
            'email' => [
                'nullable',
                'email',
                Rule::unique('members', 'email')->ignore($id),
            ],
            'photo_url' => [
                'nullable',
                'mimes:jpg, png, jpeg',
                'max:2000'
            ],
            'photo_identity_url' => [
                'nullable',
                'mimes:jpg, png, jpeg',
                'max:2000'
            ],
        ];
    }
}
