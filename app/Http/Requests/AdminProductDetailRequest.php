<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdminProductDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);

        return [
            'color' => [
                'nullable',
                'string',
            ],
            'variation' => [
                'nullable',
                'string'
            ],
            'photo_url' => [
                'nullable',
                'image',
                'max:2000'
            ],
        ];
    }
}
