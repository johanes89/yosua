<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\Configuration;

class ConfigurationComposer
{
    /**
     * The user repository implementation.
     *
     * @var \App\Repositories\UserRepository
     */

    /**
     * Create a new profile composer.
     *
     * @param  \App\Repositories\UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(
            'configuration', 
            $this->getMenus()
        );

        $view->with(
            'profile', 
            $this->getProfile()
        );

        $view->with(
            'profileMenus', 
            $this->getProfileMenus()
        );
        
    }

    public function getMenus()
    {
        $configuration = Configuration::get()->first();
        // dd($configuration);

        return $configuration;
    }

    public function getProfile()
    {
        $profile = '';
        $user = auth()->user();
        if(!empty($user)){
            $profile = (object) [
                'photo_profile' => $user->fileUrl,
                'name' => $user->name,
                'email' => $user->email,
            ];
        }

        return $profile;
    }

    public function getProfileMenus()
    {
        $profileMenus = collect();

        $profileMenus->push((object)[
            "menus" => "
            <li><a href=".route('admin-users.personalSetting')."><em class='icon ni ni-user-alt'></em><span>Pengaturan Akun</span></a></li>
            "
        ]);

        $profileMenus->push((object)[
            "menus" => "
            <li><a href=".route('admin-users.securitySetting')."><em class='icon ni ni-setting-alt'></em><span>Ganti Password</span></a></li>
            "
        ]);

        $profileMenus->push((object)[
            "menus" => "
            <li><a class='dark-switch' href='#'><em class='icon ni ni-moon'></em><span>Mode Gelap</span></a></li>
            "
        ]);

        // $profileMenus->push((object)[
            // "menus" => "
            // <li><a href='#'><em class='icon ni ni-setting-alt'></em><span>Account Setting</span></a></li>
            // "
        // ]);

        return $profileMenus;
    }
}