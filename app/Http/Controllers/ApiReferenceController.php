<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\RestrictedUser;
use App\Helpers\_function;
use App\Models\Country;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\SubDistrict;
use App\Models\ShippingMethod;
use App\Models\PaymentMethod;
use App\Models\Slideshow;
use App\Models\Category;
use App\Models\Configuration;

class ApiReferenceController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function country()
    {
        $countries = Country::where('id', '=', 100)->get();
        return response()->json($countries, 200);
    }

    public function province()
    {
        $provinces = Province::get();
        return response()->json($provinces, 200);
    }

    public function regencyById($id)
    {
        $regencies = Regency::where('province_id', '=', $id)->get();

        return response()->json($regencies, 200);
    }

    public function districtById($id)
    {
        $districts = District::where('regency_id', '=', $id)->get();

        return response()->json($districts, 200);
    }

    public function subDistrictById($id)
    {
        $subDistricts = SubDistrict::where('district_id', '=', $id)->get();

        return response()->json($subDistricts, 200);
    }

    public function shippingMethod()
    {
        $shippingMethods = ShippingMethod::get();
        return response()->json($shippingMethods, 200);
    }

    public function paymentMethod()
    {
        $paymentMethods = PaymentMethod::get();
        return response()->json($paymentMethods, 200);
    }

    public function slideshow()
    {
        $slideshow = Slideshow::get();
        return response()->json($slideshow, 200);
    }

    public function category()
    {
        $categories = Category::get();
        return response()->json($categories, 200);
    }

    public function configuration()
    {
        $configuration = Configuration::first();
        return response()->json($configuration, 200);
    }
}
