<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GroupMenu;
use App\Helpers\RestrictedUser;
use App\Helpers\_function;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\ProductDetail;
use App\Http\Requests\CheckoutRequest;
use App\Http\Requests\PaymentConfirmationRequest;
use Illuminate\Support\Str;
use App\Helpers\Media;

class ApiCartController extends Controller
{

    public function save(CheckoutRequest $request)
    {
        $generate = Str::random(8);

        $transaction = new Transaction;
        $transaction->transaction_code = Str::of($generate)->upper();
        $transaction->firstname = $request->firstname;
        $transaction->lastname = $request->lastname;
        $transaction->gender = $request->gender;
        $transaction->phone = $request->phone;
        $transaction->email = $request->email;
        $transaction->country_id = $request->country_id;
        $transaction->province_id = $request->province_id;
        $transaction->regency_id = $request->regency_id;
        $transaction->district_id = $request->district_id;
        $transaction->sub_district_id = $request->sub_district_id;
        $transaction->address = $request->address;
        $transaction->postalcode = $request->postalcode;
        $transaction->shipping_method_id = $request->shipping_method_id;
        $transaction->grand_total = $request->subTotal;
        $transaction->save();

        foreach($request->carts as $cart){
            $transactionDetail = new TransactionDetail;
            $transactionDetail->transaction_id = $transaction->id;
            $transactionDetail->product_detail_id = $cart['id'];
            $transactionDetail->qty = $cart['quantity'];
            $transactionDetail->total = $cart['total'];
            $transactionDetail->save();

            $updateProduct = ProductDetail::find($transactionDetail->product_detail_id);
            $updateProduct->qty -= $transactionDetail->qty;
            $updateProduct->save();
        }

        $summaryTransaction = Transaction::with([
            'country',
            'province',
            'regency',
            'district',
            'subDistrict',
            'shippingMethod',
            'paymentMethod',
            'transactionDetail' => function($q){
                $q->with([
                    'productDetail' => function($q){
                        $q->with([
                            'product'
                        ]);
                    }
                ]);
            }
        ])->find($transaction->id);

        $status = "Pembelian berhasil! Kami akan segera memproses pesanan anda.";

        return response()->json(['status' => $status, 'summaryTransaction' => $summaryTransaction], 200);
    }

    public function getTransaction($id)
    {
        $status = 'error';
        $message = "Maaf pesanan yang anda cari tidak ditemukan";

        $summaryTransaction = Transaction::with([
            'country',
            'province',
            'regency',
            'district',
            'subDistrict',
            'shippingMethod',
            'paymentMethod',
            'transactionDetail' => function($q){
                $q->with([
                    'productDetail' => function($q){
                        $q->with([
                            'product'
                        ]);
                    }
                ]);
            }
        ])->where('transaction_code', '=', $id)->orWhere('email', '=', $id)->get()->first();

        if(!empty($summaryTransaction)){
            $status = 'success';
            $message = "Silahkan cek pesanan dan total belanja sebelum melakukan pembayaran.";
        }

        return response()->json(['status' => $status, 'message' => $message, 'summary' => $summaryTransaction], 200);
    }

    public function paymentConfirmation(Request $request, $id)
    {
        $status = 'success';
        $message = "Terima Kasih, pembayaran anda akan segera kami proses.";

        $summaryTransaction = Transaction::with([
            'country',
            'province',
            'regency',
            'district',
            'subDistrict',
            'shippingMethod',
            'paymentMethod',
            'transactionDetail' => function($q){
                $q->with([
                    'productDetail' => function($q){
                        $q->with([
                            'product'
                        ]);
                    }
                ]);
            }
        ])->where('uuid', '=', $id)->get()->first();

        $old_path_image = $summaryTransaction->payment_slip;

        $path = 'transactions';
        if($request->hasFile('payment_slip')){
            
            $image_url = Media::imageUploadCrop ($path, 640, 640, $request->file('payment_slip'), 640, 640);
            $summaryTransaction->payment_slip = $image_url;
        }

        $summaryTransaction->payment_method_id = $request->payment_method_id;
        $summaryTransaction->status = Transaction::PAYMENT;
        $summaryTransaction->save();



        if ($summaryTransaction->payment_slip != $old_path_image) {

            if (Media::fileExists($old_path_image)) {

                Media::deleteFileUpload($old_path_image);

            }
        }

        return response()->json(['status' => $status, 'message' => $message,'summaryTransaction' => $summaryTransaction], 200);
    }
    
}
