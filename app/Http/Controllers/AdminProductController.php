<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Yajra\Datatables\Datatables;
use DB;
use App\Http\Requests\AdminProductRequest;
use App\Helpers\Media;

class AdminProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $form = 'backends.adminProducts.form';

        $action = 'backends.includes.create';
        $route = 'admin-products.store';
        $routeMethod = 'POST';

        $label = 'Tambah Produk';

        $labelTable = 'Daftar Produk';
        $table = 'backends.includes.table';
        
        $routeDatatable = route('admin-products.datatable');
        $datatableColumns = $this->getDatatableColumns();
        $fileUpload = false;

        $categoryOptions = Category::asDropdownOptions();

        return view('backends.adminProducts.index', 
        compact(
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'table',
            'routeDatatable',
            'datatableColumns',
            'fileUpload',
            'categoryOptions'
        ));
    }

    private function getDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'category_id'],
                [ 'data' => 'name'],
                [ 'data' => 'price'],
                [ 'data' => 'discount'],
                [ 'data' => 'discount_type'],
                [ 'data' => 'description'],
                [ 'data' => 'status'],
                [ 'data' => 'action', 'orderable' => false, 'searchable' => false ],
            ],
            'labelColumns' => [
                'Kategori',
                'Nama',
                'Harga',
                'Diskon',
                'Jenis Diskon',
                'Keterangan',
                'Status',
                'Aksi'
            ]
        ];
        
        return $columns;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminProductRequest $request)
    {
        try {
            DB::beginTransaction();
            $adminProduct = new Product;
            $adminProduct = $adminProduct->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-products.index')
            ->with('success_message', 'Produk Berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataReference = Product::find($id);
        $dataReferenceId = $dataReference->id;

        $form = 'backends.adminProducts.form';
        $action = 'backends.includes.edit';
        $route = 'admin-products.update';
        $routeMethod = 'PATCH';

        $label = 'Ubah Produk';
        $labelTable = 'Daftar Produk';
        
        $fileUpload = false;

        $categoryOptions = Category::asDropdownOptions();

        return view('backends.adminProducts.index', 
        compact(
            'dataReference',
            'dataReferenceId',
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'fileUpload',
            'categoryOptions'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminProductRequest $request, $id)
    {

        try {
            DB::beginTransaction();
            $adminProduct = Product::find($id);

            $adminProduct = $adminProduct->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-products.index')
            ->with('success_message', 'Produk berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $adminProduct = Product::find($id);

            $adminProduct->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        session()->flash('success_message', 'Produk berhasil dihapus');
        return ['success'];
    }

    public function datatable()
    {
        $query = Product::query();

            return Datatables::of($query)
                ->addColumn('action', function ($adminProduct) {
                    $editLink = "<a href=".route('admin-products.edit', ['admin_product' => $adminProduct->id])." class='btn btn-primary btn-wth-Product Product-wthot-bg btn-sm mb-1'><span class='Product-label'><i class='fa fa-edit'></i> </span><span class='btn-text'>edit</span></a>";

                    $deleteLink = "<a href=".route('admin-products.destroy', ['admin_product' => $adminProduct->id])." class='btn btn-danger btn-wth-Product Product-wthot-bg btn-sm delete-button mb-1'><span class='Product-label'><i class='fa fa-trash'></i> </span><span class='btn-text'>delete</span></a>";

                    $detailLink = "<a href=".route('admin-product-details.index', ['product_id' => $adminProduct->id])." class='btn btn-primary btn-wth-Product Product-wthot-bg btn-sm'><span class='Product-label'><i class='fa fa-trash'></i> </span><span class='btn-text'>detail</span></a>";

                    return $editLink . $deleteLink . $detailLink ?? null;
                })
                ->editColumn('name', function($adminProduct){
                    $detail = "<a href=".route('admin-product-details.index', ['product_id' => $adminProduct->id]).">".$adminProduct->name."</a>";

                    return $detail ?? null;
                })
                ->editColumn('discount_type', function($adminProduct){
                    return ucwords($adminProduct->discount_type) ?? null;
                })
                ->editColumn('category_id', function($adminProduct){
                    return $adminProduct->category->name ?? null;
                })
                ->editColumn('status', function($adminProduct){
                    return $adminProduct->displayStatus ?? null;
                })
                ->rawColumns(['name', 'action'])
                ->toJson();
    }
}