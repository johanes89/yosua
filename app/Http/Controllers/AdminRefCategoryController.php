<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Yajra\Datatables\Datatables;
use DB;
use App\Http\Requests\AdminCategoryRequest;

class AdminRefCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $form = 'backends.adminRefCategories.form';

        $action = 'backends.includes.create';
        $route = 'admin-ref-categories.store';
        $routeMethod = 'POST';

        $label = 'Tambah Kategori';

        $labelTable = 'Daftar Kategori';
        $table = 'backends.includes.table';
        
        $routeDatatable = route('admin-ref-categories.datatable');
        $datatableColumns = $this->getDatatableColumns();
        $fileUpload = false;

        return view('backends.adminRefCategories.index', 
        compact(
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'table',
            'routeDatatable',
            'datatableColumns',
            'fileUpload',
        ));
    }

    private function getDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'name'],
                [ 'data' => 'status'],
                [ 'data' => 'action', 'orderable' => false, 'searchable' => false ],
            ],
            'labelColumns' => [
                'Nama',
                'Status',
                'Aksi'
            ]
        ];
        
        return $columns;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminCategoryRequest $request)
    {
        try {
            DB::beginTransaction();
            $adminRefCategory = new Category;
            $adminRefCategory = $adminRefCategory->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-ref-categories.index')
            ->with('success_message', 'Kategori berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataReference = Category::find($id);
        $dataReferenceId = $dataReference->id;

        $form = 'backends.adminRefCategories.form';
        $action = 'backends.includes.edit';
        $route = 'admin-ref-categories.update';
        $routeMethod = 'PATCH';

        $label = 'Ubah Kategori';
        $labelTable = 'Kategori terdaftar';
        
        $fileUpload = false;

        return view('backends.adminRefCategories.index', 
        compact(
            'dataReference',
            'dataReferenceId',
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'fileUpload',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminCategoryRequest $request, $id)
    {

        try {
            DB::beginTransaction();
            $adminRefCategory = Category::find($id);
            $adminRefCategory = $adminRefCategory->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-ref-categories.index')
            ->with('success_message', 'Kategori berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $adminRefCategory = Category::find($id);
            $adminRefCategory->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        session()->flash('success_message', 'Kategori berhasil dihapus');
        return ['success'];
    }

    public function datatable()
    {
        $query = Category::query();

            return Datatables::of($query)
                ->addColumn('action', function ($adminRefCategory) {
                    $editLink = "<a href=".route('admin-ref-categories.edit', ['admin_ref_category' => $adminRefCategory->id])." class='btn btn-primary btn-wth-Category Category-wthot-bg btn-sm mb-1'><span class='Category-label'><i class='fa fa-edit'></i> </span><span class='btn-text'>edit</span></a>";

                    $deleteLink = "<a href=".route('admin-ref-categories.destroy', ['admin_ref_category' => $adminRefCategory->id])." class='btn btn-danger btn-wth-Category Category-wthot-bg btn-sm delete-button'><span class='Category-label'><i class='fa fa-trash'></i> </span><span class='btn-text'>delete</span></a>";
                    return $editLink . $deleteLink;
                })
                ->editColumn('status', function($adminRefCategory){
                    return $adminRefCategory->displayStatus ?? null;
                })
                ->rawColumns(['action'])
                ->make(true);
    }
}