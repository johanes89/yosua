<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GroupMenu;
use App\Helpers\RestrictedUser;
use App\Helpers\_function;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return view('frontends.home.index');
    }
}
