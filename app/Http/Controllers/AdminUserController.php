<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdminUser;
use App\Models\GroupMenu;
use App\Models\UserMenuPrivilege;
use Yajra\Datatables\Datatables;
use App\Http\Requests\AdminUserRequest;
use App\Http\Requests\AdminUpdatePersonalSettingRequest;
use App\Http\Requests\AdminUpdatePasswordRequest;
use DB;
use Illuminate\Support\Arr;
use App\Helpers\Media;
use App\Helpers\_function;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataReference = new AdminUser;
        $listMenuRegistered = [];

        $form = 'backends.adminUsers.form';
        $action = 'backends.includes.create';
        $route = 'admin-users.store';
        $routeMethod = 'POST';

        $label = 'Create User';
        $message = 'Tips: Privilege is set only for Administrator';
        $labelTable = 'Lists User';
        $table = 'backends.includes.table';
        
        $routeDatatable = route('admin-users.datatable');
        $datatableColumns = $this->getDatatableColumns();
        $fileUpload = false;


        return view('backends.adminUsers.index', 
        compact(
            'dataReference',
            'listMenuRegistered',
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'message',
            'labelTable',
            'table',
            'routeDatatable',
            'datatableColumns',
            'fileUpload',
        ));
    }
    
    private function getDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'name'],
                [ 'data' => 'email'],
                [ 'data' => 'email_verified_at'],
                [ 'data' => 'profile_photo_url'],
                [ 'data' => 'status'],
                [ 'data' => 'action', 'orderable' => false, 'searchable' => false ],
            ],
            'labelColumns' => [
                'Name',
                'Email',
                'Email Verified',
                'Photo Profile',
                'Status',
                'Action'
            ]
        ];
        
        return $columns;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminUserRequest $request)
    {

        try {
            DB::beginTransaction();
            $adminUser = new AdminUser;
            $adminUser->name = $request->name;
            $adminUser->email = $request->email;
            $adminUser->status = AdminUser::ACTIVE;
            // $randomPassword = $adminUser->randomPassword();

            $adminUser->password = 'admin123';
            
            $adminUser->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-users.index')
            ->with('success_message', 'User baru berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataReference = AdminUser::find($id);
        $dataReferenceId = $dataReference->id;

        $form = 'backends.adminUsers.form';
        $action = 'backends.includes.edit';
        $route = 'admin-users.update';
        $routeMethod = 'PATCH';

        $label = 'Edit User';
        $message = 'Tips: Privilege is set only for Administrator';
        $labelTable = 'Lists User';
        $fileUpload = false;


        return view('backends.adminUsers.index', 
        compact(
            'dataReference',
            'dataReferenceId',
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'message',
            'labelTable',
            'fileUpload',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUserRequest $request, $id)
    {

        try {
            DB::beginTransaction();
            $adminUser = AdminUser::find($id);
            $oldName = $adminUser->name;
            $oldEmail = $adminUser->email;
            
            $adminUser = $adminUser->saveFromRequest($request);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-users.index')
            ->with('success_message', 'User berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $adminUser = AdminUser::find($id);

            $adminUser->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        session()->flash('success_message', 'User successfully removed');
        return ['success'];
    }

    public function personalSetting()
    {
        $menuId = 'personal-setting';
        $userId = auth()->user()->id;
        $dataReference = AdminUser::find($userId);
        $dataReferenceId = $dataReference->id;

        $action = 'backends.adminUsers.account-setting';
        $label = 'Edit User';
        $message = 'Not interested in custom validation feedback messages or writing JavaScript to change form behaviors? All good, you can use the browser defaults. Try submitting the form below.';
        $labelTable = 'Lists User';
        $route = 'admin-users.updatePersonalSetting';
        $routeMethod = 'PATCH';
        $form = 'backends.adminUsers.personal-setting';
        $fileUpload = true;

        return view('backends.adminUsers.index', 
        compact(
            'menuId',
            'dataReference',
            'dataReferenceId',
            'action',
            'label',
            'message',
            'labelTable',
            'route',
            'routeMethod',
            'form',
            'fileUpload',
        ));
    }

    public function updatePersonalSetting(AdminUpdatePersonalSettingRequest $request)
    {
        try {
            DB::beginTransaction();
            $userId = auth()->user()->id;
            $adminUser = AdminUser::find($userId);
            $old_path_image = $adminUser->profile_photo_url;
            
            $adminUser = $adminUser->saveFromRequest($request);

            if ($adminUser->profile_photo_url != $old_path_image) {

                if (Media::fileExists($old_path_image)) {

                    Media::deleteFileUpload($old_path_image);
    
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-users.personalSetting')
            ->with('success_message', 'Users successfully updated');
    }

    public function securitySetting()
    {
        $menuId = 'security-setting';
        $userId = auth()->user()->id;
        $dataReference = AdminUser::find($userId);
        $dataReferenceId = $dataReference->id;

        $action = 'backends.adminUsers.account-setting';
        $label = 'Edit User';
        $message = 'Not interested in custom validation feedback messages or writing JavaScript to change form behaviors? All good, you can use the browser defaults. Try submitting the form below.';
        $labelTable = 'Lists User';
        $route = 'admin-users.updatePassword';
        $routeMethod = 'PATCH';
        $form = 'backends.adminUsers.security-setting';
        $fileUpload = true;

        return view('backends.adminUsers.index', 
        compact(
            'menuId',
            'dataReference',
            'dataReferenceId',
            'action',
            'label',
            'message',
            'labelTable',
            'route',
            'routeMethod',
            'form',
            'fileUpload',
        ));
    }

    public function updatePassword(AdminUpdatePasswordRequest $request)
    {
        try {
            DB::beginTransaction();
            $userId = auth()->user()->id;
            $adminUser = AdminUser::find($userId);
            
            $adminUser = $adminUser->saveFromRequest($request);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-users.securitySetting')
            ->with('success_message', 'Users successfully updated');
    }

    public function datatable()
    {
        $user = auth()->user();

        $query = AdminUser::query();

        if (request()->ajax()) {
            return Datatables::of($query)
                ->addColumn('action', function ($adminUser) {
                    $editLink = "<a href=".route('admin-users.edit', ['admin_user' => $adminUser->id])." class='btn btn-primary btn-wth-icon icon-wthot-bg btn-sm mb-1'><span class='icon-label'><i class='fa fa-edit'></i> </span><span class='btn-text'>edit</span></a>";

                    $deleteLink = "<a href=".route('admin-users.destroy', ['admin_user' => $adminUser->id])." class='btn btn-danger btn-wth-icon icon-wthot-bg btn-sm delete-button'><span class='icon-label'><i class='fa fa-trash'></i> </span><span class='btn-text'>delete</span></a>";
                    return $editLink . $deleteLink;
                })
                ->editColumn('profile_photo_url', function($adminUser){
                    return $adminUser->fileImageThumb ?? null;
                })
                ->addColumn('status', function($adminUser){
                    return $adminUser->displayStatus ?? null;
                })

                ->rawColumns(['action'])
                ->toJson();
        }
    }
}
