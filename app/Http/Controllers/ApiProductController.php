<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GroupMenu;
use App\Helpers\RestrictedUser;
use App\Helpers\_function;
use App\Models\Product;
use App\Models\ProductDetail;

class ApiProductController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $products = Product::with([
            'category',
            'productDetail'
        ])
        ->paginate(16);
        return response()->json(['products' => $products], 200);
    }

    public function detail($id)
    {
        $product = Product::with([
            'category',
            'productDetail'
        ])
        ->find($id);
        
        return response()->json(['product' => $product], 200);
    }

    public function productByCategory($id)
    {
        $products = Product::with([
            'category',
            'productDetail'
        ])
        ->where('category_id', '=', $id)
        ->orderBy('id', 'desc')
        ->limit(3)
        ->get();

        return response()->json(['products' => $products], 200);
    }

    public function productSearch($type, $key)
    {
        $products = Product::with([
            'category',
            'productDetail'
        ])
        ->where('name', 'like', '%'. $key.'%')
        ->orderBy('id', 'desc')
        ->paginate(16);

        if($type == 'searchCategory'){
            $products = Product::with([
                'category',
                'productDetail'
            ])
            ->where('category_id', '=', $key)
            ->orderBy('id', 'desc')
            ->paginate(16);
        }
        
        return response()->json(['products' => $products], 200);
    }
}
