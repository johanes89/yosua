<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsAdministratorAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if(
            $user->isOwner ||
            $user->isChiefAdministrator ||
            $user->isAdministrator
        ){
            return $next($request);
        }
   
        return redirect(RouteServiceProvider::HOME)->with('error',"You don't have admin access.");
    }
}
