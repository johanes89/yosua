<?php

namespace App\Helpers;

use Image;
use Storage;
use Embed;
use File;

class Media{

    public static function imageUpload($path, $width, $height, $fileUpload)
    {
        $width = !empty($width)?$width:1024;
        $height = !empty($height)?$height:1024;

        $filename = md5(uniqid().time()).'.'.$fileUpload->getClientOriginalExtension();
        $image_url = $path . '/' . $filename;

        $image = Image::make($fileUpload);

        $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            // $constraint->upsize();
        });
        
        if (!Storage::disk('public')->exists($path)) {
            // Jika folder tidak ada, maka create folder
            Storage::disk('public')->makeDirectory($path, 0755, false, false);
        }

        $image->save(public_path() . '/' . 'storage' . '/' .$image_url);

        return $image_url;
    }

    public static function imageUploadCrop($path, $width, $height, $fileUpload, $cropW, $cropH)
    {
        $width = !empty($width)?$width:1024;
        $height = !empty($height)?$height:1024;

        $filename = md5(uniqid().time()).'.'.$fileUpload->getClientOriginalExtension();
        $image_url = $path . '/' . $filename;

        $image = Image::make($fileUpload);
        $image->resize($width, $height, function ($constraint) {
            // $constraint->aspectRatio();
            $constraint->upsize();
        });
        $image->resizeCanvas($cropW, $cropH, 'center', false, 'ffffff');    
        
        if (!Storage::disk('public')->exists($path)) {
            // Jika folder tidak ada, maka create folder
            Storage::disk('public')->makeDirectory($path, 0755, false, false);
        }

        $image->save(public_path() . '/' . 'storage' . '/' .$image_url);

        return $image_url;
    }

    public static function embedVideo($videoUrl, $width, $height)
    {
        $embed = Embed::make($videoUrl)->parseUrl();

        if (!$embed){
            return '';
        }

        if(empty($width) && !empty($height)){

            $embed->setAttribute(['height' => $height]);

        }else if(!empty($width) && empty($height)){

            $embed->setAttribute(['width' => $width]);

        }else if(!empty($width) && !empty($height)){

            $embed->setAttribute(['width' => $width, 'height' => $height]);

        }

        return $embed->getHtml();
    }

    public static function documentUpload($path, $fileUpload)
    {

        if (!File::isDirectory($path)) {
            // Jika folder tidak ada, maka create folder
            Storage::makeDirectory($path);
        }

        $filename = Storage::disk('public')->putFile($path, $fileUpload);

        return $filename;
    }

    public static function getImageUrl($filepath)
    {
        $path = url($filepath);
        return $path;
    }

    public static function fileExists($path)
    {
        return Storage::disk('public')->exists($path);
    }

    public static function deleteFileUpload($path)
    {
        return Storage::disk('public')->delete($path);
    }


}