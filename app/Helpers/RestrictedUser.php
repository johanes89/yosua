<?php

namespace App\Helpers;

use Image;
use Storage;
use Embed;
use File;
use App\Models\GroupMenu;
use App\Models\UserMenuPrivilege;

class RestrictedUser{

    public static function authorizationUserMenu($user, $menu)
    {
        $allowed = false;
        
        $restrictedUser = self::restricted($user);

        if($restrictedUser){
            $allowed = UserMenuPrivilege::where('user_id', '=', $user->id)
            ->where('menu_id', '=', $menu->id)->exists();
        }else if(!$restrictedUser){
            $allowed = true;
        }

        return $allowed;
    }

    public static function notAllowedUser()
    {
        $user = auth()->user();
        $notAllowed = false;

        if($user->isMember){

            $notAllowed = true;

        }
        return $notAllowed;
    }

    public static function restricted($user)
    {
        $restricted = false;
        if(
            $user->isSpecialEditor ||
            $user->isEditor ||
            $user->isAuthor

        ){
            $restricted = true;
        }

        return $restricted;
    }

    public static function restrictedOwner($user)
    {
        $restricted = false;
        if(
            $user->isOwner

        ){
            $restricted = true;
        }

        return $restricted;
    }

    public static function restrictedAdministrator($user)
    {
        $restricted = false;
        if(
            $user->isOwner ||
            $user->isChiefAdministrator ||
            $user->isAdministrator

        ){
            $restricted = true;
        }

        return $restricted;
    }

}