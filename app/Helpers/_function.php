<?php

namespace App\Helpers;
use App\Models\LogActivity;
use Illuminate\Http\Request;

class _function{

    const LABEL = 0;
    const BASIC_CONTENT = 1;
    const ADVANCED_CONTENT = 2;
    const EXTERNAL_LINK = 3;

    public static function locateUrl($url, $typeUrl)
    {
        $generate_url = '';

        if($typeUrl == self::BASIC_CONTENT){

            $generate_url = route('admin-contents.index', ['menu' => $url]);

        }else if($typeUrl == self::ADVANCED_CONTENT){

            $generate_url = route($url.'.index');

        }else if($typeUrl == self::EXTERNAL_LINK){

            $generate_url = url($url);

        }

        return $generate_url;
    }

    public static function createLog($user, $type, $action, $table, $activity)
    {
        $createLog = new LogActivity;
        $createLog->user_id = $user->id;
        $createLog->name = $user->name;
        $createLog->email = $user->email;
        $createLog->role = $user->userRole->role->name;
        $createLog->type = $type;
        $createLog->action = $action;
        $createLog->table = $table;
        $createLog->activity = $activity;
        $createLog->ip_address = \Request::ip();
        $createLog->user_agent = \Request::server('HTTP_USER_AGENT');
        $createLog->save();

    }

    public static function getUserDataAgent($activity)
    {
        $mac = substr(exec('getmac'), 0, 17);
        $hostname = gethostname();
        $message = $hostname.' '.$activity." from mac: ".$mac;
        return $message;
    }

}