<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('product_detail_id');
            $table->unsignedSmallInteger('qty')->default(0)->nullable();
            $table->decimal('total', 14, 2)->default(0)->nullable();
            $table->timestamps();

            $table->foreign('transaction_id')
            ->references('id')->on('transactions')
            ->onDelete('restrict');

            $table->foreign('product_detail_id')
            ->references('id')->on('product_details')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
