<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->string('transaction_code')->unique()->comment('auto rendering');
            $table->string('firstname');
            $table->string('lastname')->nullable();
            $table->enum('gender', ['L', 'P'])->nullable();
            $table->string('phone')->nullable()->comment('no telepon utama');
            $table->string('email')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->unsignedBigInteger('province_id')->nullable();
            $table->unsignedBigInteger('regency_id')->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
            $table->unsignedBigInteger('sub_district_id')->nullable();
            $table->string('address')->nullable();
            $table->string('postalcode')->nullable();
            $table->unsignedBigInteger('shipping_method_id')->nullable();
            $table->string('expedition')->nullable();
            $table->decimal('shipping_cost', 14, 2)->default(0)->nullable();
            $table->string('receipt_code')->nullable()->comment('nomor resi pengiriman');
            $table->decimal('grand_total', 14, 2)->default(0)->nullable();
            $table->unsignedBigInteger('payment_method_id')->nullable();
            $table->string('payment_slip')->nullable()->comment('bukti transfer');
            $table->unsignedTinyInteger('status')->default(0)
            ->comment('0=waiting, 1=proccessed, 2=payment, 3=shipping, 4=finish, 99=canceled');
            $table->timestamps();

            $table->foreign('country_id')
            ->references('id')->on('ref_countries')
            ->onDelete('restrict');

            $table->foreign('province_id')
            ->references('id')->on('ref_provinces')
            ->onDelete('restrict');
            
            $table->foreign('regency_id')
            ->references('id')->on('ref_regencies')
            ->onDelete('restrict');

            $table->foreign('district_id')
            ->references('id')->on('ref_districts')
            ->onDelete('restrict');

            $table->foreign('sub_district_id')
            ->references('id')->on('ref_sub_districts')
            ->onDelete('restrict');

            $table->foreign('shipping_method_id')
            ->references('id')->on('ref_shipping_methods')
            ->onDelete('restrict');

            $table->foreign('payment_method_id')
            ->references('id')->on('ref_payment_methods')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
