<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_countries', function (Blueprint $table) {
            $table->id();
            $table->string('iso')->nullable();
            $table->string('name');
            $table->string('iso3')->nullable();
            $table->unsignedInteger('numcode')->default(0)->nullable();
            $table->unsignedInteger('phonecode')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_countries');
    }
}
