<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(resource_path('json/roles.json'));
        $data = json_decode($json, true);

        foreach ($data as $obj) {
            $existing = Role::find($obj['id']);

            if (empty($existing->id)) {
                $existing = new Role;
            }

            $existing->fill($obj);
            $existing->save();
        }
        /**
            * Owner = Superadmin = Memiliki hak akses penuh terhadap website, bisa management chief administrator, dan Configurasi Website
            * Chief Administrator = Memiliki hak akses untuk memanage website dibawah Owner, dan bisa management administrator  
            * Administrator = Memiliki hak akses untuk memanage website dibawah Chief Administrator, dan bisa management Special Editor, Editor, dan Author
            * Special Editor & Editor = Memiliki hak akses untuk management Author, dan untuk publish suatu konten
            * Author = Memiliki hak akses untuk mengisi konten, dengan status unpublished
            */
    }
}
